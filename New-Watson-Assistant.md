# Welcome to the new Watson Assistant

Objective for Exercise:

- Create an instance of new Watson Assistant.
- Create a Banking Assistant.

## Overview

The new Watson Assistant experience, focused on using **actions** to build customer conversations, is designed to make it simple enough for anyone to build a virtual assistant. Building, testing, publishing, and analyzing your assistant can all now be done in one simple and intuitive interface.

- New **navigation** provides a workflow for building, previewing, publishing, and analyzing your assistant.

- Each assistant has a **home page** with a task list to help you get started.

- Build conversations with **actions**, which represent the tasks you want your assistant to help your customers with. Each action contains a series of steps that represent individual exchanges with a customer.

- A new way to **publish** lets you review and debug your work in a draft environment before going live to your customers.

- Use a new suite of **analytics** to improve your assistant. Review which actions are being completed to see what your customers want help with, determine if your assistant understands and addresses customer needs, and decide how can you make your assistant better.

## Create an instance of new Watson Assistant

To access the resources and services that the IBM Cloud provides, you need an IBM Cloud account.

If you already have an IBM Cloud account, you can directly move on to creating Watson Assistant Service. Otherwise, please follow this lab [create an IBM Cloud account](https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBM-CC0100EN-SkillsNetwork/labs/IBMCloud_accountCreation/CreateIBMCloudAccount.md.html).

### Task 1: Add a Watson Assistant Service Instance

We need to add an instance of the Watson Assistant service to your account. This will be used later to create chatbot. 

1. Click on the IBM Cloud [Catalog](https://cloud.ibm.com/catalog)

    From the catalog page that loads, Click on the services, select the AI category and then select the Watson Assistant service from the list.

    <img src="images/WA1.png" width="600" style="border: solid 1px grey; padding-left=20px" />

2. In the Watson Assistant setup page, choose a region depending on where you are located. For example, here we have chosen Dallas. Verify that the Lite plan is selected, and then click Create.

    > Note: If you created a Watson Assistant instance in the past using the Lite plan, you won't be allowed to create a second instance. You can use the instance you already created for this course or you'll need to delete your previous instance and create a new one.

    <img src="images/WA2.png" width="600" style="border: solid 1px grey; padding-left=20px" />

3. Once the service is created you will see the below page, from where you can launch Watson Assistant. Click on the **Launch Watson Assistant** button  to access the web application that will allow you to create chatbots. 

    <img src="images/WA3.png" width="600" />


### Task 2: New Watson Assistant instance

Some instances are automatically provisioned with the new Watson Assistant.


<img src="images/new-instance.PNG" width="600" style="border: solid 1px grey; padding-left=20px" />


If your instance is still using the **classic Watson Assistant**, you can switch to the new experience by following these steps:

1. From the Watson Assistant interface, click the **User Panel** icon to open your account menu.

<img src="images/user-icon1.png" width="600" style="border: solid 1px grey; padding-left=20px" />

2. Select **Switch to new experience** from the account menu.

<img src="images/new-experience.png" width="200" >

3. Click **Switch** to confirm that you want to start using the new experience.

<img src="images/switch.png" width="600" style="border: solid 1px grey; padding-left=20px" />

> Note: You won't lose any work if you switch to the new experience, and you can switch back to the classic experience at any time by clicking Switch to classic experience from the account menu.


## Create a Banking Assistant

Give your assistant the name **Banking Chatbot**. Choose the language it uses to respond to your customers, For this lab please select **English** and click on **Create**.

<img src="images/b1.png" width="600" style="border: solid 1px grey; padding-left=20px" />

You are on the **Home** page of your Assistant, you can also see the Assistant details here. Please click on the **Actions** represented by the **message** icon, see the below screenshot for reference.

<img src="images/b2.PNG" width="600" style="border: solid 1px grey; padding-left=20px" />

Now, you can create your first action, with actions you can help your customers accomplish their goals. Please click on **Create a new action** button.

> Note: An **action** represents a discrete outcome you want your assistant to be able to accomplish in response to a user's request. An action comprises the interaction between a customer and the assistant about a particular question or request.

<img src="images/b3.png" width="600" style="border: solid 1px grey; padding-left=20px" />

### Creating and editing an action

When you create a new action, Watson Assistant prompts you for an example of the customer input that starts the action. This text is also used as the default name for the action, but you can edit the action name later.

<img src="images/b4.png" width="600" style="border: solid 1px grey; padding-left=20px" />

Type _I want to withdraw money_ and then click **Save** to create the action.

> Note: Initially, you only need to specify one example of typical user input that starts the action. You can add more examples of user input later if you want to.

After you create the action, the action editor opens. In the Step 1 is taken field, leave the default value of without conditions. This step is always required for any withdrawal. 

In the Assistant says field, type _Withdraw from which account?_.

<img src="images/b5.PNG" width="600" style="border: solid 1px grey; padding-left=20px" />

Now, Click **Define customer response** and select **Options** because we will be asking the user to select from a list of predefined choices( Saving or Checking)

<img src="images/b6.PNG" width="600" style="border: solid 1px grey; padding-left=20px" />

In the Option 1 field, type **Savings**. As soon as you enter a value for option 1, a field appears for options 2. Navigate to Option 2 and type **Checking**.


<img src="images/b7.png" width="600" style="border: solid 1px grey; padding-left=20px" />

Click **Apply** to save the customer response.

> Note: If the customer already specified the account type previously; for example, if the initial customer input was _I want to withdraw money from my **savings** account_ then it won't present the options to you. But if you want the options to always be present you can do that by clicking on the **Settings** icon and selecting the **Always ask for this information, regardless of the earlier message**.

<img src="images/new3.png" width="600" style="border: solid 1px grey; padding-left=20px" />

<img src="images/new2.png" width="600" style="border: solid 1px grey; padding-left=20px" />

Now, click **New step**. In the Step 2 is taken field, select with conditions. The Conditions section expands.

<img src="images/b8.png" width="600" style="border: solid 1px grey; padding-left=20px" />

By default, a condition is automatically created based on the action variable stored by the previous step (Withdraw from which account?). However, by default it is checking for a value of **Savings**.

<img src="images/b9.png" width="600" style="border: solid 1px grey; padding-left=20px" />

Now, click on the **Add condition** and select **Checking**. For both conditions **Saving** and **Checking** the assistant should ask _How much do you want to withdraw?_.

<img src="images/b10.png" width="600" style="border: solid 1px grey; padding-left=20px" />

Also, change from **All** option to **Any**. In the **Assistant says** field, type _How much do you want to withdraw?_.

<img src="images/b11.png" width="1400" style="border: solid 1px grey; padding-left=20px" />

Click **Define customer response**. This time we need the customer to specify a monetary amount, so select **Currency**. There are no more details you need to specify for a currency amount, so it is immediately added to the step.

<img src="images/new1.png" width="700" style="border: solid 1px grey; padding-left=20px" />

#### Finishing the action

We now have all the information we need. For our example, we're not going to implement any real logic for making a withdrawal, but we can send a final message summing up what we're doing.

To do this, we need to insert action variables (representing customer responses from previous steps) into our response. At run time, these action variables will be replaced with the actual values supplied by the customer.

Click **New step**. Now we need to build a confirmation message that says **"OK, we will withdraw _amount_ from your _account_type_ account.Thank you."**

To create this response, type the text of the message in the Assistant says field, but in place of the variable values, click the **Insert a variable** icon to insert references to action variables:

- For **amount**, select 2. **How much do you want to withdraw?**.
- For **account_type**, select 1. **Withdraw from which account?**.

<img src="images/b12.png" width="1000" style="border: solid 1px grey; padding-left=20px" />


Because this is the last step in the action, you don't need to specify any customer response.

Please click on **Preview** button to verify your chatbot responses.

<img src="images/b13.PNG" width="300" style="border: solid 1px grey; padding-left=20px" />

### Thank you for completing this lab.

## Author(s)

[Shubham Kumar Yadav](https://www.linkedin.com/in/shubham-kumar-yadav-14378768/)

















